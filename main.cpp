#include <iostream>
#include <cmath>
#include <cstring>
#include <stdlib.h>
#include <math.h>

//menentukan input price
const double cdMinimumPrice =0;
const double cdMaximumPrice =10;

//menentukan input quality
const double cdMinimumQuality =0;
const double cdMaximumQuality =10;
using namespace std;


//menentukan funsgifuzzy
class CFuzzyFunction
{
protected :
	double dLeft, dRight;
	char   cType;
	char*  sName;

public:
	CFuzzyFunction(){};
	virtual ~CFuzzyFunction(){ delete [] sName; sName=NULL;}

	virtual void
	setInterval(double l,
	            double r)
	{dLeft=l; dRight=r;}

        virtual void
	setMiddle( double dL=0,
	           double dR=0)=0;

	virtual void
	setType(char c)
	{ cType=c;}

	virtual void
	setName(const char* s)
	{
	  sName = new char[strlen(s)+1];
	  strcpy(sName,s);
	}

	bool
	isDotInInterval(double t)
	{
		if((t>=dLeft)&&(t<=dRight)) return true; else return false;
	}

	char getType(void)const{ return cType;}

        void
        getName() const
	{
		cout<<sName<<endl;
	}

	virtual double getValue(double t)=0;
};
//funsgi berbentuk segitiga
class CTriangle : public CFuzzyFunction
{
private:
	double dMiddle;

public:
	void
	setMiddle(double dL, double dR)
	{
		dMiddle=dL;
	}

	double
	getValue(double t)
	{
		if(t<=dLeft)
			return 0;
		else if(t<dMiddle)
			return (t-dLeft)/(dMiddle-dLeft);
		else if(t==dMiddle)
			return 1.0;
		else if(t<dRight)
		    return (dRight-t)/(dRight-dMiddle);
		else
			return 0;
	}
};
//fungsi berbentuk trapesium
class CTrapezoid : public CFuzzyFunction
{
private:
	double dLeftMiddle, dRightMiddle;

public:
    void
	setMiddle(double dL, double dR)
	{
		dLeftMiddle=dL; dRightMiddle=dR;
	}

	double
	getValue(double t)
	{
		if(t<=dLeft)
	       return 0;
		else if(t<dLeftMiddle)
			return (t-dLeft)/(dLeftMiddle-dLeft);
		else if(t<=dRightMiddle)
			return 1.0;
		else if(t<dRight)
			return (dRight-t)/(dRight-dRightMiddle);
		else
		    return 0;
	}
};

int
main(void)
{
    //Membuat himpunan fuzzy dari price
	CFuzzyFunction *PriceSet[3];

	PriceSet[0] = new CTriangle;
	PriceSet[1] = new CTriangle;
	PriceSet[2] = new CTriangle;

	PriceSet[0]->setInterval(-4,4);
	PriceSet[0]->setMiddle(0,0);
	PriceSet[0]->setType('t');
	PriceSet[0]->setName("low_price");

	PriceSet[1]->setInterval(1,9);
	PriceSet[1]->setMiddle(5,5);
	PriceSet[1]->setType('t');
	PriceSet[1]->setName("good");

	PriceSet[2]->setInterval(6,14);
	PriceSet[2]->setMiddle(10,10);
	PriceSet[2]->setType('t');
	PriceSet[2]->setName("expensive");

	//membuat himpunan fuzzy dari quality
    CFuzzyFunction *QualitySet[2];

	QualitySet[0] = new CTrapezoid;
	QualitySet[1] = new CTrapezoid;

	QualitySet[0]->setInterval(-2,3);
	QualitySet[0]->setMiddle(0,1);
	QualitySet[0]->setType('r');
	QualitySet[0]->setName("BAD");

	QualitySet[1]->setInterval(7,12);
	QualitySet[1]->setMiddle(9,10);
	QualitySet[1]->setType('r');
	QualitySet[1]->setName("Delicious");

	//menyiapkan variable untuk menghitung probabilitas
	double dPrice,dQuality;
	double probPrice[3],probQuality[2];
     for(int i=0; i<3; i++)
	  {
	  probPrice[i]=0;
	  }
    for(int i=0; i<2; i++)
	  {
	  probQuality[i]=0;
	  }

    do
	{
	  cout<<"\nImput the value->"; cin>>dPrice;
    //mengecek apakah input price berada dalam range
	  if(dPrice<cdMinimumPrice) continue;
	  if(dPrice>cdMaximumPrice) continue;
    //memprint keterangan dalam setiap fungsi price
      for(int i=0; i<3; i++)
	  {
		 cout<<"\nThe dot="<<dPrice<<endl;
		 if(PriceSet[i]->isDotInInterval(dPrice))
			 cout<<"In the interval";
		 else
			 cout<<"Not in the interval";
		 cout<<endl;

         cout<<"The name of function is"<<endl;
		 PriceSet[i]->getName();
		 cout<<"and the membership is=";

		 cout<<PriceSet[i]->getValue(dPrice)<<endl;
         probPrice[i]=PriceSet[i]->getValue(dPrice);
         cout<<probPrice[i]<<endl;

	  }

	  cout<<"\nImput the value->"; cin>>dQuality;
       //mengecek apakah input price berada dalam range
	  if(dQuality<cdMinimumQuality) continue;
	  if(dQuality>cdMaximumQuality) continue;
     //memprint keterangan dalam setiap fungsi quality
      for(int i=0; i<2; i++)
	  {
		 cout<<"\nThe dot="<<dQuality<<endl;
		 if(QualitySet[i]->isDotInInterval(dQuality))
			 cout<<"In the interval";
		 else
			 cout<<"Not in the interval";
		 cout<<endl;

         cout<<"The name of function is"<<endl;
		 QualitySet[i]->getName();
		 cout<<"and the membership is=";

		 cout<<QualitySet[i]->getValue(dQuality)<<endl;

        probQuality[i]=QualitySet[i]->getValue(dQuality);
         cout<<probQuality[i]<<endl;
	  }
    //RULEBASE
    double valueOutput[3];
    cout<<"prob rule base"<<endl;
     for(int i=0; i<3; i++)
	  {
        valueOutput[i]=0;
        if(i==0)
            {
            //rulebase jika makanan bad atau(MAX) low price
            if(probPrice[i]>probQuality[i])
                {valueOutput[i]=probPrice[i];}
            else
                {valueOutput[i]=probQuality[i];}
            cout<<i<<" "<<valueOutput[i]<<endl;
            }
        if(i==1)
            {
            //rulebase jika  good price
            valueOutput[i]=probPrice[i];
            cout<<i<<" "<<valueOutput[i]<<endl;
            }
        if(i==2)
            {
             //rulebase jika makanan delicious atau(MAX) expensive
            if(probPrice[i]>probQuality[i-1])//ingat quality cuma 2
                {valueOutput[i]=probPrice[i];}
            else
                {valueOutput[i]=probQuality[i-1];}
            cout<<i<<" "<<valueOutput[i]<<endl;

            }
	  }
    //perhitungan outpur menggunakan metode center yang menghitung luas segitiga
    //dimana ada 3 yaitu 0-10,10-20,20-30
    //dengan cente 5,15,25
    //perhitungan x1 dan x2 (fungsi implikasi)
    //https://tutorkeren.com/artikel/menerapkan-fungsi-implikasi.htm
    cout<<"x1 dan x2"<<endl;
    double x[3][2];
        for(int i=0; i<3; i++)
	  {
        for(int j=0; j<2; j++)
            {
            x[i][j]=0;
            if(j==0)
            {x[i][j]=5*valueOutput[i]+i*10;}
            if(j==1)
            {x[i][j]=-1*(5*valueOutput[i]-i*10-j*10);}
            cout<<i<<" "<<j<<" "<<x[i][j]<<endl;
            }
	  }
    //https://tutorkeren.com/artikel/mengkomposisikan-semua-output.htm
    //DEFUZZIFIKASI
    //Menghitung luas
    //https://tutorkeren.com/artikel/defuzzification.htm
    cout<<"LUAS"<<endl;
    double A[3][3],area=0;
    for(int i=0; i<3; i++)
	  {
        for(int j=0; j<3; j++)
            {
                if(j==0)
                {
                    A[i][j]=((x[i][j]-i*10)*valueOutput[i])/2;
                }
                if(j==1)
                {
                    A[i][j]=(x[i][j]-x[i][j-1])*valueOutput[i];
                }
                if(j==2)
                {
                    A[i][j]=(((i+1)*10-x[i][j-1])*valueOutput[i])/2;
                }

                if(valueOutput[i]==0)//Luas hasilnya 0 jika probability 0
                {
                    A[i][j]=0;
                }
                cout<<i<<" "<<j<<" "<<A[i][j]<<endl;
            area=area+A[i][j];
            }
      }
       //Menghitung MOMEN
         double M[3][3],moment=0;
    cout<<"MOMEN"<<endl;
    for(int i=0; i<3; i++)
	  {
        for(int j=0; j<3; j++)
            {
                double y1=0,y2=0;//untuk pengurangan integral
                if(j==0)
                {
                    y1=0.0667*pow(i*10,3)-i*pow(i*10,2);
                    y2=0.0667*pow(x[i][j],3)-i*pow(x[i][j],2);
                    M[i][j]=y2-y1;
                }
                if(j==1)
                {
                    y1=0.25*pow(x[i][j-1],2);
                    y2=0.25*pow(x[i][j],2);
                    M[i][j]=y2-y1;
                }
                if(j==2)
                {
                    y1=(i+1)*pow(x[i][j-1],2)-0.0667*pow(x[i][j-1],3);
                    y2=(i+1)*pow((i+1)*10,2)-0.0667*pow((i+1)*10,3);
                    M[i][j]=y2-y1;

                }
                if(valueOutput[i]==0)//momen hasilnya 0 jika probability 0
                {
                    M[i][j]=0;
                }
                cout<<i<<" "<<j<<" "<<M[i][j]<<endl;
            moment=moment+M[i][j];
            }
      }

    //Hasil nilai output tip
    double tip=0;
    cout<<"momen"<<moment<<endl;
    cout<<"area"<<area<<endl;
    tip=moment/area;
    cout<<"anda sebaiknya memberi tip sebesar"<<tip<<endl;
    }


	while(true);

	return EXIT_SUCCESS;
}
